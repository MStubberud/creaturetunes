﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public List<Creature> creatures;
    public Texture2D[] cursorTexture;
    public Sprite[] backgrounds;
    public int cursorSize = 200;
    public bool showCursor = true;
    public int progressPoints = 0;

    private Texture2D currentTexture;
    private int curSizeX;
    private int curSizeY;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;

            for (int i = 0; i < creatures.Count; ++i)
            {
                creatures[i].unlocked = false;
            }
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this
            Destroy(gameObject);
        }

        // Keep object between scene loading
        DontDestroyOnLoad(gameObject);

        curSizeX = cursorSize;
        curSizeY = cursorSize;
        Cursor.visible = false;
        currentTexture = cursorTexture[0];
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }
    }

    void OnGUI()
    {
        if (showCursor)
        {
            GUI.DrawTexture(new Rect(Event.current.mousePosition.x, Event.current.mousePosition.y, curSizeX, curSizeY), currentTexture);
        }
    }

    public void EnterHighlight()
    {
        instance.currentTexture = instance.cursorTexture[1];
    }

    public void ExitHighlight()
    {
        instance.currentTexture = instance.cursorTexture[0];
    }

    public void SetBG(int index)
    {
        GameObject.FindGameObjectWithTag("Background").GetComponent<SpriteRenderer>().sprite = instance.backgrounds[index];
    }

    public void UnlockCreature(Creature newCreature)
    {
        // Index of creature with same name as input
        int find = FindCreatureWithName(newCreature.creatureName);

        // If the creature exists
        if (find != -1)
        {
            // Use index of found creature to set to unlocked
            creatures[find].unlocked = true;
            progressPoints += 10;
        }
    }

    public bool IsCreatureUnlocked(string name)
    {
        // Index of creature with same name as input
        int find = FindCreatureWithName(name);

        // If the creature exists
        if (find != -1)
        {
            // Use index of found creature to return unlock status
            return creatures[find].unlocked;
        }

        // Otherwise return false
        return false;
    }

    public AudioClip[] GetCreatureTunes(string name)
    {
        // Index of creature with same name as input
        int find = FindCreatureWithName(name);

        // If the creature exists
        if (find != -1)
        {
            // Use index of found creature to return unlock status
            return creatures[find].clips;
        }

        // Otherwise return array with one empty clip
        return new AudioClip[1];
    }

    public Sprite GetCreatureImage(string name)
    {
        // Index of creature with same name as input
        int find = FindCreatureWithName(name);

        return creatures[find].GetComponent<SpriteRenderer>().sprite;
    }

    public Color GetCreatureColor(string name)
    {
        // Index of creature with same name as input
        int find = FindCreatureWithName(name);

        return creatures[find].GetComponent<SpriteRenderer>().color;
    }

    private int FindCreatureWithName(string name)
    {
        // Index of creature with same name as input
        int index = -1;

        // Look through all currently unlocked creatures or until similarly named creature is found
        for (int i = 0; i < creatures.Count && index == -1; ++i)
        {
            // If a creature has the same name as the new one
            if (name == creatures[i].creatureName)
            {
                // Store a found creatures index, also ends the loop
                index = i;
            }
        }

        return index;
    }

    public void Quit()
    {
        Application.Quit();
    }
}