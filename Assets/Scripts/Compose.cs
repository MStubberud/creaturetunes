﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compose : MonoBehaviour
{
    public AudioSource upperTone;
    public AudioSource lowerTone;
    public AudioSource chord;
    public AudioSource bass;
    public AudioSource beat;

    public AudioSource intro;
    public AudioClip introClip;

    private AudioSource tempAud;

    private AudioClip[] upperTones;
    private AudioClip[] lowerTones;
    private AudioClip[] chords;
    private AudioClip[] bassTones;
    private AudioClip[] beats;

    private AudioClip[] creatureClips;

    const int MAXTAKT = 32;

    int bonusPoints = 5;
    bool stop = true;
    int step;
    string currentName;
    string prevName;


    GameManager gameManager;

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        tempAud = GetComponents<AudioSource>()[5];
        currentName = "";
        prevName = "";
        upperTones = new AudioClip[MAXTAKT];
        lowerTones = new AudioClip[MAXTAKT];
        chords =     new AudioClip[MAXTAKT];
        bassTones =  new AudioClip[MAXTAKT];
        beats =      new AudioClip[MAXTAKT];

        upperTone = GetComponents<AudioSource>()[0];
        lowerTone = GetComponents<AudioSource>()[1];
        chord = GetComponents<AudioSource>()[2];
        bass = GetComponents<AudioSource>()[3];
        beat = GetComponents<AudioSource>()[4];
        step = 0;

        intro.clip = introClip;
        intro.Play();

        SetStartAlpha("Buster");
        SetStartAlpha("Chigh");
        SetStartAlpha("Coggy");
        SetStartAlpha("Dorry");
        SetStartAlpha("Exri");
        SetStartAlpha("Aki");
        SetStartAlpha("Giraya");
        SetStartAlpha("Feldspor");
    }

    private void SetStartAlpha(string tagName)
    {
        if (!gameManager.IsCreatureUnlocked(tagName))
        {
            Color temp = GameObject.FindGameObjectWithTag(tagName).GetComponent<Image>().color;
            temp.a = 0.2f;
            GameObject.FindGameObjectWithTag(tagName).GetComponent<Image>().color = temp;
        }
    }

    public void SetActive(string name)
    {
        if (gameManager.IsCreatureUnlocked(name))
        {
            prevName = currentName;
            currentName = name;
            creatureClips = gameManager.GetCreatureTunes(name);

            GameObject temp2 = GameObject.FindGameObjectWithTag(currentName);
            setYellowColor(temp2);

            if (prevName != "")
            {
                GameObject temp = GameObject.FindGameObjectWithTag(prevName);
                setWhiteColor(temp);
            }
        }
    }

    public void insertUpperTone(int x)
    {
        upperTones[x] = creatureClips[0];
        tempAud.clip = creatureClips[0];
        tempAud.Play();
        
        GameObject temp = GameObject.FindGameObjectWithTag("Upper").transform.GetChild(x).gameObject;
        setSprite(temp);
    }

    public void insertLowerTone(int x)
    {
        lowerTones[x] = creatureClips[1];
        tempAud.clip = creatureClips[1];
        tempAud.Play();

        GameObject temp = GameObject.FindGameObjectWithTag("Lower").transform.GetChild(x).gameObject;
        setSprite(temp);
    }

    public void insertChord(int x)
    {
        chords[x] = creatureClips[2];
        tempAud.clip = creatureClips[2];
        tempAud.Play();

        GameObject temp = GameObject.FindGameObjectWithTag("Chord").transform.GetChild(x).gameObject;
        setSprite(temp);
    }

    public void insertBassTone(int x)
    {
        bassTones[x] = creatureClips[3];
        tempAud.clip = creatureClips[3];
        tempAud.Play();

        GameObject temp = GameObject.FindGameObjectWithTag("Bass").transform.GetChild(x).gameObject;
        setSprite(temp);
    }

    public void insertBeat(int x)
    {
        beats[x] = creatureClips[4];
        tempAud.clip = creatureClips[4];
        tempAud.Play();

        GameObject temp = GameObject.FindGameObjectWithTag("Beat").transform.GetChild(x).gameObject;
        setSprite(temp);
    }

    public void Play()
    {
        StopAllCoroutines();
        StartCoroutine(PlayIterative());
    }

    public void Stop()
    {
        upperTone.Stop();
        
        lowerTone.Stop();
        
        chord.Stop();
        
        bass.Stop();
        
        beat.Stop();

        step = 0;

        stop = true;
    }

    public void Clear()
    {
        upperTones = new AudioClip[MAXTAKT];
        lowerTones = new AudioClip[MAXTAKT];
        chords =     new AudioClip[MAXTAKT];
        bassTones =  new AudioClip[MAXTAKT];
        beats =      new AudioClip[MAXTAKT];

        GameObject temp = GameObject.FindGameObjectWithTag("Upper");
        for (int i = 0; i < temp.transform.childCount; ++i)
        {
           clearSprite(temp.transform.GetChild(i).gameObject);
        }

        temp = GameObject.FindGameObjectWithTag("Lower");
        for (int i = 0; i < temp.transform.childCount; ++i)
        {
            clearSprite(temp.transform.GetChild(i).gameObject);
        }

        temp = GameObject.FindGameObjectWithTag("Chord");
        for (int i = 0; i < temp.transform.childCount; ++i)
        {
            clearSprite(temp.transform.GetChild(i).gameObject);
        }

        temp = GameObject.FindGameObjectWithTag("Bass");
        for (int i = 0; i < temp.transform.childCount; ++i)
        {
            clearSprite(temp.transform.GetChild(i).gameObject);
        }

        temp = GameObject.FindGameObjectWithTag("Beat");
        for (int i = 0; i < temp.transform.childCount; ++i)
        {
            clearSprite(temp.transform.GetChild(i).gameObject);
        }
    }

    private IEnumerator PlayIterative()
    {
        stop = false;
        step = 0;
        while (step < MAXTAKT && stop == false)
        {
            upperTone.clip = upperTones[step];
            upperTone.Play();

            lowerTone.clip = lowerTones[step];
            lowerTone.Play();

            chord.clip = chords[step];
            chord.Play();

            bass.clip = bassTones[step];
            bass.Play();

            beat.clip = beats[step];
            beat.Play();

            step++;

            if (stop == false)
                yield return new WaitForSeconds(0.417f);
            else
                yield break;
        }

        FindObjectOfType<GameManager>().progressPoints += bonusPoints;
        bonusPoints = 0;
    }

    private void setSprite(GameObject temp)
    {
        temp.GetComponent<Image>().sprite = gameManager.GetCreatureImage(currentName);
    }

    private void clearSprite(GameObject temp)
    {
        temp.GetComponent<Image>().sprite = null;
    }

    private void setYellowColor(GameObject temp)
    {
        temp.GetComponent<Image>().color = Color.yellow;
    }

    private void setWhiteColor(GameObject temp)
    {
        temp.GetComponent<Image>().color = Color.white;
    }
}
