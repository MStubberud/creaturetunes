﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetVisibility : MonoBehaviour {

    float alpha;

    // Use this for initialization
    void Start () {
        alpha = GetComponent<Image>().color.a;
        Color temp = GetComponent<Image>().color;
        temp.a = 0.0f;
        GetComponent<Image>().color = temp;
    }

    public void ToggleVisible()
    {
        Color temp = GetComponent<Image>().color;
        if (temp.a == alpha)
        {
            temp.a = 0.0f;
        }
        else
        {
            temp.a = alpha;
        }

        GetComponent<Image>().color = temp;
    }

}
