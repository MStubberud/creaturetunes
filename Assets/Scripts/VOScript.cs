﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VOScript : MonoBehaviour {

    private int i;
    private LevelManager levelManager;
	// Use this for initialization
	void Start ()
    {
        FindObjectOfType<GameManager>().showCursor = false;
        i = 0;
        levelManager = FindObjectOfType<LevelManager>();
        StartCoroutine(PlayIterative());
    }

    private IEnumerator PlayIterative()
    {
        VOManager.Instance.Play(i++);

        while (i < 15)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0) || !VOManager.Instance.GetComponent<AudioSource>().isPlaying)
            {
                VOManager.Instance.PlayInterrupt(i++);
            }

            yield return null;
        }

        FindObjectOfType<GameManager>().progressPoints = 0;
        FindObjectOfType<GameManager>().showCursor = true;
        levelManager.LoadLevel(2);
    }
}
