﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BGMPlayer : MonoBehaviour
{
    public static BGMPlayer instance = null;
    public AudioClip[] tracks;
    public AudioSource collectCreature;
    public AudioClip collectCreatureClip;

    private AudioSource[] players;
    private int activePlayer = 0;
    private int inactivePlayer = 1;

    void Awake()
    {
        //Check if instance already exists
        if (instance == null)
        {
            //if not, set instance to this
            instance = this;
        }

        //If instance already exists and it's not this:
        else if (instance != this)
        {
            //Then destroy this
            Destroy(gameObject);
        }

        // Keep object between scene loading
        DontDestroyOnLoad(gameObject);

        players = GetComponents<AudioSource>();
    }

    private void OnEnable()
    {
        //Tell the 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        //Tell the 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {

        // Menu
        if (scene.buildIndex == 0)
        {
            SwitchPlayerIndices();
            players[activePlayer].clip = tracks[0];

            StartCoroutine(FadeOutWithFadeIn());
        }

        // Intro & Composer
        else if (scene.buildIndex == 5 || scene.buildIndex == 1)
        {
            SwitchPlayerIndices();

            StartCoroutine(FadeOut());
        }

        // House & Village
        else if (scene.buildIndex == 2 || scene.buildIndex == 3)
        {
            if (players[activePlayer].clip != tracks[1] || !players[activePlayer].isPlaying)
            {
                SwitchPlayerIndices();
                players[activePlayer].clip = tracks[1];

                StartCoroutine(FadeOutWithFadeIn());
            }
        }

        // Forest
        else if (scene.buildIndex == 4)
        {
            SwitchPlayerIndices();
            players[activePlayer].clip = tracks[2];

            StartCoroutine(FadeOutWithFadeIn());
        }

        // Ruins
        else if (scene.buildIndex == 6)
        {
            SwitchPlayerIndices();
            players[activePlayer].clip = tracks[3];

            StartCoroutine(FadeOutWithFadeIn());
        }

        // Ruins
        else if (scene.buildIndex == 11)
        {
            SwitchPlayerIndices();
            players[activePlayer].clip = tracks[4];

            StartCoroutine(FadeOutWithFadeIn());
        }
    }

    private void SwitchPlayerIndices()
    {
        int temp = activePlayer;
        activePlayer = inactivePlayer;
        inactivePlayer = temp;
    }

    private IEnumerator FadeOut()
    {
        while (players[inactivePlayer].volume > 0.01f)
        {
            players[inactivePlayer].volume -= 1.0f * Time.deltaTime;
            yield return null;
        }

        players[inactivePlayer].Stop();
    }

    private IEnumerator FadeOutWithFadeIn()
    {
        while (players[inactivePlayer].volume > 0.01f)
        {
            players[inactivePlayer].volume -= 1.0f * Time.deltaTime;
            yield return null;
        }

        players[inactivePlayer].Stop();

        StartCoroutine(FadeIn());
    }

    private IEnumerator FadeIn()
    {
        players[activePlayer].volume = 0.0f;
        players[activePlayer].Play();

        while (players[activePlayer].volume < 0.30f)
        {
            players[activePlayer].volume += 1.0f * Time.deltaTime;
            yield return null;
        }
    }
}
