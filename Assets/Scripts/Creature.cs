﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creature : MonoBehaviour
{
    public string creatureName;
    public string progName;
    public AudioClip[] clips;
    public bool unlocked;

    void Awake()
    {
        if (FindObjectOfType<GameManager>().IsCreatureUnlocked(creatureName))
        {
            Destroy(gameObject);
        }
        
        if (Random.Range(0, 2) != 0)
        {
            Destroy(gameObject);
        }
    }

    // Sends itself to gamemanager for unlock
    void OnMouseDown()
    {
        GameManager gameManager = FindObjectOfType<GameManager>();
        gameManager.UnlockCreature(this);
        if (gameManager.progressPoints == 10)
        {
            BGMPlayer bgmPlayer = FindObjectOfType<BGMPlayer>();
            bgmPlayer.collectCreature.clip = bgmPlayer.collectCreatureClip;
            bgmPlayer.collectCreature.Play();
            bgmPlayer.collectCreature.volume = 1;
            bgmPlayer.collectCreature.loop = false;
        }
        Destroy(gameObject);

    }
}
