﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroBGDelete : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(deleteAfterSeconds(8.1f));
	}

    private IEnumerator deleteAfterSeconds(float target)
    {
        float dt = 0;
        while (dt < target)
        {
            dt += Time.deltaTime;
            yield return null;
        }

        Destroy(gameObject);
    }
}
