﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public AudioClip ruinsBoop;

    private GameManager gameManager;
    private AudioSource audioCue;

    void Start()
    {
        audioCue = GetComponent<AudioSource>();
    }

    public void LoadLevel(int index)
    {
        if (FindObjectOfType<GameManager>().progressPoints < 10)
        {
            if (index != 2 && index != 3 && index != 4 && index != 7 && index != 8 && index != 9 && index != 10)
            {
                if (!audioCue.isPlaying)
                {
                    audioCue.Play();
                }
            }
            else
            {
                SceneManager.LoadScene(index);
            }
        }
        else if (FindObjectOfType<GameManager>().progressPoints < 55)
        {
            audioCue.clip = ruinsBoop;
            if (index == 6)
            {
                audioCue.Play();
            }
            else
            {
                SceneManager.LoadScene(index);
            }
        }
        else
        {
            SceneManager.LoadScene(index);
        }
    }
}
