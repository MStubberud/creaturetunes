﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextReading : MonoBehaviour {

    string upcomingText = "There is, however, one exception; a misunderstood and strange person by the name William, who lives together with a creature in a tree. He creates beautiful sound waves to make peace.";
    Text text;
    bool exit = false;
    public LevelManager levelManager;
    AudioSource source;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        source = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
        if (exit && Input.GetMouseButtonUp(0))
        {
            levelManager.LoadLevel(1);
        }
        if (Input.GetMouseButtonUp(0))
        {
            text.text = upcomingText;
            exit = true;
            source.Play();

        }
	}
}
